# WAM - FE Prueba

El proyecto está desarrollado con Nuxt. 
He decidido usarlo en vez de Vue a secas porque me ha parecido interesante enseñarlo.

# Instalación

1. Clonar repositorio
2. Instalar NodeJS.
3. En la carpeta del proyecto lanzar npm install
4. lanzar npm run dev

# Sobre la Prueba

# Estilos

Tras un debate interno, no he utilizado Bootstrap. Al final es una librería que todo el mundo conoce y sabe utilizar. Me parecía excesivo utilizarla
cuando no iba a ser del todo eficiente y el uso quizá iba a ser contraproducente.

De los preprocesadores mencionados en la prueba me he decantado por LESS, es con el que más cómodo me siento, aunque para la prueba tampoco me he querido
explayar demasiado con las funcionalidades, he usado unas pocas variables para los colores y anidado de clases.

# Funcionalidades

He dividio el proyecto en dos páginas: Main y ConfirmationPage. Lo iba a hacer en principio todo en la main pero como estaba utilizando Nuxt, he decidido crear una página aparte para la confirmación de la compra.

# Main

Hay dos componentes, por defecto está el customerData activado, cuando el usuario rellena el formulario y le da al botón de continuar se emiten los parámetros
y se lanza la función que los guarda en el data y cambia el componente. Una vez el usuario confirma el pago, se envían los datos del usuario y se cambia a la página de confirmación. Los datos se envían a través de axios.post, se podría usar fetch() pero axios me resulta más práctico y completo.

En esta página se lanza una petición axios.get para recuperar la información. He simulado que tarda 1s el servidor en responder para mostrar un spinner que carga mientras se reciben los datos.
No he podido utilizar la información sobre la imagen que recibia desde la api proporcionada porque la url era "certificado.svg" y eso no llevaba a ningún sitio.
Si realizas un bind con el src de un img no te coge la ruta absoluta, por lo que lo he descartado.

# Componentes

He intentado que fueran todos completamente reutilizables.

# Menu

Iba a configurarlo de manera que se recibieran los datos desde una api y se generase el menú de manera dinámica, pero al final es una lista.

# Language selector

Esto lo he separado como componente, aunque forma parte del menú, porque a mi entender esta sección debería de tener lógica dentro y no estaría dentro de proporcio menú. Aunque no hay lógica ninguna aplicada, es para entender el concepto.

# Logo

Es el logo de WAM en SVG.

# Stepper

Es un componente que espera recibir el número del paso que está actual y dentro de el se deberían de recibir los pasos que estén configurados.
Dependiendo de estos, hay otro componente dentro que se llama "Steps" que se encarga de renderizar los pasos configurados.

# Button

Botones específicos para utilizar cuando existan varios pasos a seguir.
Se puede configurar el texto, el tipo de botón y si es el botón a renderizar se utiliza para avanzar o para retroceder en el sistema.

# Dialog

Es una caja donde se puede configurar un título y una descripción, además, tiene un slot para introducir el contenido. He decidido darle el nombre al slot de #body para no dejarlo como default.

# Spinner

Spinner que se puede cambiar el color, con una animación básica de keyframes y que se puede utilizar con un simple booleano mientras se carga la información.
